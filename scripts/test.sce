exec loader.sce
MED_Init();

scripts_path = get_absolute_file_path("test.sce");
function path = MEDGetRootPath()
  path = fullpath(scripts_path + '/..');
endfunction

cd("tests/unit_tests");
exec("testUtils.sci");
exec("testIArray.tst");
exec("testDArray.tst");
exec("testUMesh.tst");
exec("testCMesh.tst");
exec("testDField.tst");
exec("testLoader.tst");

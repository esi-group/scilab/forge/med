#include "MEDCouplingMemArray.hxx"
#include "MEDCouplingRefCountObject.hxx"
#include <algorithm>

void IArray_setValues(ParaMEDMEM::DataArrayInt* dataArrayInt, int* values, int nbTuples, int nbComponents)
{
    //dataArrayInt->useArray((const int*) values, false, ParaMEDMEM::CPP_DEALLOC, nbTuples, nbComponents);
    dataArrayInt->alloc(nbTuples, nbComponents);
    std::copy(values, values + nbTuples*nbComponents, dataArrayInt->getPointer());
    dataArrayInt->declareAsNew();
}

void IArray_getValues(ParaMEDMEM::DataArrayInt* dataArrayInt, int** values, int* nbTuples, int* nbComponents)
{
    *nbTuples = dataArrayInt->getNumberOfTuples();
    *nbComponents = dataArrayInt->getNumberOfComponents();
    *values = dataArrayInt->getPointer();
}

void DArray_setValues(ParaMEDMEM::DataArrayDouble* dataArrayDouble, double* values, int nbTuples, int nbComponents)
{
    //dataArrayDouble->useArray((const double*) values, false, ParaMEDMEM::CPP_DEALLOC, nbTuples, nbComponents);
    dataArrayDouble->alloc(nbTuples, nbComponents);
    std::copy(values, values + nbTuples*nbComponents, dataArrayDouble->getPointer());
    dataArrayDouble->declareAsNew();
}

void DArray_getValues(ParaMEDMEM::DataArrayDouble* dataArrayDouble, double** values, int* nbTuples, int* nbComponents)
{
    *nbTuples = dataArrayDouble->getNumberOfTuples();
    *nbComponents = dataArrayDouble->getNumberOfComponents();
    *values = dataArrayDouble->getPointer();
}

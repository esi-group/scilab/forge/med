function values_by_comp = getValuesFromMED(file_name, group_name)
  values_by_comp = struct();
  
  meshLvl_0 = MED_ReadUMeshFromFile(file_name);
  mesh_name = Mesh_getName(meshLvl_0);
  
  // Les segments sont au niveau 0 (car il n'y a pas de mailles faces ou volumes)  
  // Les noeuds au niveau 1
  segs_level = 0;
  nodes_level = 0;

  group_mesh = MED_ReadUMeshFromGroups(file_name, mesh_name, nodes_level, [group_name]);
  
  d_mesh0_coords = PtSet_getCoords(meshLvl_0);
  d_group_coords = PtSet_getCoords(group_mesh);
  [ok, i_group_nodes] = DArray_areIncludedInMe(d_mesh0_coords, d_group_coords, 1E-5);

  if ~ok then
    error("Node in group not in mesh!")
  end
    
  group_nodes = IArray_getValues(i_group_nodes);
  if size(group_nodes, '*') <> 1 then
    error("One node is expected in group of node");
  end
  
  node_id = group_nodes(1);
  
  i_cells_near_node = PtSet_getCellIdsLyingOnNodes(meshLvl_0, group_nodes, %f);
  
  cells_near_node = IArray_getValues(i_cells_near_node);
  
  if size(cells_near_node, '*') <> 1 then
      error("One cell is expected near the group of node");
  end

  cell_id = cells_near_node(1);

  // R�cup�re les noms des champs contenus dans le fichier MED
  field_names = MED_GetAllFieldNamesOnMesh(file_name, mesh_name)
  d_values_by_comp = [];
  
  for i = 1:size(field_names, '*')  
    field_name = field_names(i);
    field_types = MED_GetTypesOfField(file_name, mesh_name, field_name);
          
    if size(field_types, '*') == 1 then
        field_type = field_types(1);
    elseif size(field_types, '*') == 0 then
        // Bug dans Salome 7.3.0, le type ELNO n'est pas r�cup�r� par l'API basique de MEDLoader 
        field_type = ON_GAUSS_NE;
    else
        error("Only one fieldtype is expected");
    end
  
    field = MED_ReadField(field_type, file_name, mesh_name, 0, field_name, -1, -1);
    
    // Champ aux noeuds par �l�ment (ELNO)
    if field_type == ON_GAUSS_NE then
        // R�cup�re les noeuds de la cellule
        cell_nodes = Mesh_getNodeIdsOfCell(meshLvl_0, cell_id);
      
        // D�termine l'index du noeud qui nous int�resse parmi les noeuds de la cellule
        node_id_in_cell = find(cell_nodes == node_id);        
      
        // D�termine � quel index du field se trouve la valeur de notre noeud
        d_tuple_ids = Field_computeTupleIdsToSelectFromCellIds(field, cells_near_node);
        tuple_ids = IArray_getValues(d_tuple_ids);
      
        tuple_id = tuple_ids(node_id_in_cell);
    elseif field_type == ON_CELLS then
        // Valeur constante dans l'�l�ment
        tuple_id = cell_id;
    elseif fieldtype == ON_NODES then
        // Valeur au noeud
        tuple_id = node_id;
    end
    
    // Noms des composantes
    d_field_array = DField_getArray(field);
    comp_names = Array_getVarsOnComponent(d_field_array);
    
    // R�cup�re les valeurs pour chaque composante et les stocke dans un dictionnaire
    for j = 1:size(comp_names, '*')
        comp_name = comp_names(j);
        if find(fieldnames(values_by_comp) == comp_name) <> [] then
            error(msprintf("The component %s has already been read from a previous field.", comp_name));
        end
        if tuple_id <> [] then
            comp_value = DArray_getIJ(d_field_array, tuple_id, j-1);
        else
            comp_value = [];
        end
        values_by_comp(comp_name) = comp_value;        
    end
  end
endfunction


MED_Init();

file_name = "test2a.med"
valuesAa = getValuesFromMED(file_name, "PA")
valuesBa = getValuesFromMED(file_name, "PB")
  
file_name = "test2b.med"
valuesAb = getValuesFromMED(file_name, "PA")
valuesBb = getValuesFromMED(file_name, "PB")

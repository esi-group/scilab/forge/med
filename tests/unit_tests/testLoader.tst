// =============================================================================
// Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
// Copyright (C) 2013 - Scilab Enterprises - Simon MARCHETTO
//
//  This file is distributed under the same license as the Scilab package.
// =============================================================================

// <-- CLI SHELL MODE -->
// <-- ENGLISH IMPOSED -->

exec(fullfile(MEDGetRootPath(), "tests/unit_tests/testUtils.sci"));

EPS = 0.001;

function test_RW_UMesh1D()
    expected_mesh = createUMesh1D();
    medFileName = fullfile(TMPDIR, "test_RW_UMesh1D.med");
    MED_WriteUMesh(medFileName, expected_mesh, %T);
    mesh = MED_ReadUMeshFromFile(medFileName, Mesh_getName(expected_mesh), 0);
    assert_checktrue(Mesh_isEqual(mesh, expected_mesh, EPS));
endfunction

function test_RW_UMesh2D()
    expected_mesh = createUMesh2D();
    medFileName = fullfile(TMPDIR, "test_RW_UMesh2D.med");
    MED_WriteUMesh(medFileName, expected_mesh, %T);
    mesh = MED_ReadUMeshFromFile(medFileName, Mesh_getName(expected_mesh), 0);
    assert_checktrue(Mesh_isEqual(mesh, expected_mesh, EPS));
endfunction

function test_RW_UMesh2DCurve()
    expected_mesh = createUMesh2DCurve();
    medFileName = fullfile(TMPDIR, "test_RW_UMesh2D.med");
    MED_WriteUMesh(medFileName, expected_mesh, %T);
    mesh = MED_ReadUMeshFromFile(medFileName, Mesh_getName(expected_mesh), 0);
    assert_checktrue(Mesh_isEqual(mesh, expected_mesh, EPS));
endfunction

function test_RW_UMesh3DSurf()
    expected_mesh = createUMesh3DSurf();
    medFileName = fullfile(TMPDIR, "test_RW_UMmesh3DSurf.med");
    MED_WriteUMesh(medFileName, expected_mesh, %T);
    mesh = MED_ReadUMeshFromFile(medFileName, Mesh_getName(expected_mesh), 0);
    assert_checktrue(Mesh_isEqual(mesh, expected_mesh, EPS));
endfunction

function test_RW_UMesh3D()
    expected_mesh = createUMesh3DSurf();
    medFileName = fullfile(TMPDIR, "test_RW_UMesh3D.med");
    MED_WriteUMesh(medFileName, expected_mesh, %T);
    mesh = MED_ReadUMeshFromFile(medFileName, Mesh_getName(expected_mesh), 0);
    assert_checktrue(Mesh_isEqual(mesh, expected_mesh, EPS));
endfunction

function test_RW_DField_cells()
    expected_field = createDFieldOnCells();
    DField_setTime(expected_field, 2., 0, 1);
    medFileName = fullfile(TMPDIR, "test_RW_DField_cells.med");
    MED_WriteField(medFileName, expected_field, %T);
    mesh = Field_getMesh(expected_field);
    field = MED_ReadFieldCell(medFileName, Mesh_getName(mesh), 0, Field_getName(expected_field), 0, 1);
    assert_checktrue(Field_isEqual(field, expected_field, EPS, EPS));
endfunction

function test_RW_DField_cells_AlreadyWrittenMesh()
    medFileName = fullfile(TMPDIR, "test_RW_DField_cells_AlreadyWrittenMesh.med");
    [expected_field, fieldName, mesh] = createDFieldOnCells();
    MED_WriteField(medFileName, expected_field, %T);

    DField_setTime(expected_field, 10., 8, 9);
    darray = DField_getArray(expected_field);
    DArray_setIJ(darray, 0, 0, 12345.67890314);
    MED_WriteFieldUsingAlreadyWrittenMesh(medFileName, expected_field);

    //DField_setTime(expected_field, 10.14, 18, 19);
    //darray = DField_getArray(expected_field);
    //DArray_setIJ(darray, 0, 0, -1111111111111.);
    //MED_WriteFieldUsingAlreadyWrittenMesh(medFileName, expected_field);

    meshName = Mesh_getName(mesh);
    field = MED_ReadFieldCell(medFileName, meshName, 0, fieldName, 8, 9);

    DField_setTime(expected_field, 10., 8, 9);
    darray = DField_getArray(expected_field);
    DArray_setIJ(darray, 0, 0, 12345.67890314);

    assert_checktrue(Field_isEqual(field, expected_field, EPS, EPS));
endfunction

function [meshes, multiMeshName] = createAndWriteMeshPartition(medFileName)
    meshes = createMultiMesh();
    multiMeshName = "multiMesh";
    MED_WriteUMeshesPartition(medFileName, multiMeshName, meshes, %T);
endfunction

function test_getMeshFamiliesNames()
    medFileName = fullfile(TMPDIR, "test_getMeshFamiliesNames.med");
    [meshes, multiMeshName] = createAndWriteMeshPartition(medFileName);

    families = MED_GetMeshFamiliesNames(medFileName, ["submesh1"]);
    //assert_checkequal(families, ["Family_2"]);
endfunction

function test_getMeshOnGroupFamiliesNames()
    medFileName = fullfile(TMPDIR, "test_getMeshOnGroupFamiliesNames.med");
    [meshes, multiMeshName] = createAndWriteMeshPartition(medFileName);

    families = MED_GetMeshOnGroupFamiliesNames(medFileName, multiMeshName, "UMesh3D");
    assert_checkequal(families, ["Family_-2" "Family_-3" "Family_-4" "Family_-5"]);
endfunction

function test_readUMeshFromFamilies()
    medFileName = fullfile(TMPDIR, "test_readUMeshFromFamilies.med");
    [meshes, multiMeshName] = createAndWriteMeshPartition(medFileName);

    mesh = MED_ReadUMeshFromFamilies(medFileName, multiMeshName, 0, ["Family_-6"; "Family_-2"]);
    //assert_checktrue(Mesh_isEqual(meshes(1), mesh, EPS));
endfunction

function test_getMeshGroupsNames()
    medFileName = fullfile(TMPDIR, "test_getMeshGroupsNames.med");
    [meshes, multiMeshName] = createAndWriteMeshPartition(medFileName);

    groups = MED_GetMeshGroupsNames(medFileName, multiMeshName);
    assert_checkequal(groups, ["UMesh3D" "submesh1" "submesh2" "submesh3"]);
endfunction

function test_getMeshOnFamilyGroupsNames()
    medFileName = fullfile(TMPDIR, "test_getMeshOnFamilyGroupsNames.med");
    [meshes, multiMeshName] = createAndWriteMeshPartition(medFileName);

    groups = MED_GetMeshOnGroupFamiliesNames(medFileName, multiMeshName, ["Family_2"]);
    //assert_checkequal(groups, ["UMesh3D" "submesh1"]);
endfunction

function test_readUMeshFromGroups()
    medFileName = fullfile(TMPDIR, "test_readUMeshFromGroups.med");
    [meshes, multiMeshName] = createAndWriteMeshPartition(medFileName);

    mesh = MED_ReadUMeshFromGroups(medFileName, multiMeshName, 0, ["UMesh3D"]);
    assert_checktrue(Mesh_isEqual(meshes(1), mesh, EPS));

    mesh = MED_ReadUMeshFromGroups(medFileName, multiMeshName, 0, ["submesh1"]);
    assert_checktrue(Mesh_isEqual(meshes(2), mesh, EPS));

    mesh = MED_ReadUMeshFromGroups(medFileName, multiMeshName, 0, ["submesh2"]);
    assert_checktrue(Mesh_isEqual(meshes(3), mesh, EPS));

    mesh = MED_ReadUMeshFromGroups(medFileName, multiMeshName, 0, ["submesh3"]);
    assert_checktrue(Mesh_isEqual(meshes(4), mesh, EPS));
endfunction

function [fields, iterations, meshName, fieldName] = createAndWriteMultiField(medFileName)
    [field, fieldName, mesh] = createDFieldOnCells();
    meshName = Mesh_getName(mesh);

    MED_WriteUMesh(medFileName, mesh, %T);

    DField_setTime(field, 0., 1, 2);
    field1 = DField_cloneWithMesh(field, %T);
    MED_WriteFieldUsingAlreadyWrittenMesh(medFileName, field1);

    DField_applyFunc(field, "2*x");
    DField_setTime(field, 0.01, 3, 4);
    field2 = DField_cloneWithMesh(field, %T);
    MED_WriteFieldUsingAlreadyWrittenMesh(medFileName, field2);

    DField_applyFunc(field, "2*x/3");
    DField_setTime(field, 0.02, 5, 6);
    field3 = DField_cloneWithMesh(field, %T);
    MED_WriteFieldUsingAlreadyWrittenMesh(medFileName, field3);

    fields = list(field, field1, field2, field3);
    iterations = list(createIntPair(1, 2), createIntPair(3, 4), createIntPair(5, 6));
endfunction

function test_readFieldsOnSameMesh()
    medFileName = fullfile(TMPDIR, "test_readFieldsOnSameMesh.med");
    [expected_fields, iterations, meshName, fieldName] = createAndWriteMultiField(medFileName);

    fields = MED_ReadFieldsOnSameMesh(ON_CELLS, medFileName, meshName, 0, fieldName, iterations);

    assert_checkequal(size(fields), 3);
    //assert_checktrue(Field_isEqual(fields(1), expected_fields(1), EPS, EPS));
    //assert_checktrue(Field_isEqual(fields(2), expected_fields(2), EPS, EPS));
    //assert_checktrue(Field_isEqual(fields(3), expected_fields(3), EPS, EPS));
endfunction

function test_getCellFieldIterations()
    medFileName = fullfile(TMPDIR, "test_getCellFieldIterations.med");
    [expected_fields, expected_iterations, meshName, fieldName] = createAndWriteMultiField(medFileName);

    iterations = MED_GetCellFieldIterations(medFileName, meshName, fieldName);

    assert_checkequal(size(iterations), 3);
    assert_checkequal(iterations(1), expected_iterations(1));
    assert_checkequal(iterations(2), expected_iterations(2));
    assert_checkequal(iterations(3), expected_iterations(3));
endfunction

function test_getTypesOfField()
    medFileName = fullfile(TMPDIR, "test_getTypesOfField.med");
    [expected_field, fieldName, mesh] = createDFieldOnCells();
    MED_WriteField(medFileName, expected_field, %T);
    fieldTypes = MED_GetTypesOfField(medFileName, Mesh_getName(mesh), fieldName);
    assert_checkequal(fieldTypes, [ON_CELLS]);
endfunction

test_RW_UMesh1D();
test_RW_UMesh2DCurve();
test_RW_UMesh2D();
test_RW_UMesh3DSurf();
test_RW_UMesh3D();
test_RW_DField_cells();
test_RW_DField_cells_AlreadyWrittenMesh();
test_getMeshFamiliesNames();
test_getMeshOnGroupFamiliesNames();
test_readUMeshFromFamilies();
test_getMeshGroupsNames();
test_getMeshOnFamilyGroupsNames();
test_readUMeshFromGroups();
test_readFieldsOnSameMesh();
test_getCellFieldIterations();
test_getTypesOfField();


